//
//  main.m
//  BHouse
//
//  Created by Megha Bambra on 2015-02-12.
//  Copyright (c) 2015 Bhouse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
