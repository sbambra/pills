//
//  ViewController.h
//  BHouse
//
//  Created by Satraj Bambra on 2015-02-12.
//  Copyright (c) 2015 Bhouse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>

@property(strong,nonatomic) IBOutlet UIScrollView *scrollView;

@end

