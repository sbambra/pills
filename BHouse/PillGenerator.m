//
//  PillGenerator.m
//  BHouse
//
//  Created by Satraj Bambra on 2015-02-12.
//  Copyright (c) 2015 Bhouse. All rights reserved.
//

#import "PillGenerator.h"

#define PILL_HEIGHT 30.0f
#define HORIZONTAL_MARGIN 15.0f
#define INITIAL_VERTICAL_MARGIN 10.0f
#define PILL_WIDTH_MARGIN 28.0f
#define PILL_SEPERATOR_MARGIN 6.0f
#define PILL_FONT [UIFont systemFontOfSize:15.0f]
#define PILL_COLOR [UIColor colorWithRed:50.0/255.0 green:155.0/255.0 blue:194.0/255.0 alpha:1.0];

#define ANIMATION_DURATION_PILL_IN 0.20f
#define PILL_ENTRANCE_DELAY 0.12f

@implementation PillGenerator

static float delay;

+ (void)addPills:(NSArray *)pills toScrollView:(UIScrollView *)scrollView {
    
    delay = 0.0f;
    CGFloat initialOriginX = HORIZONTAL_MARGIN;
    float currentWidthCount = HORIZONTAL_MARGIN;
    CGFloat initialOriginY = INITIAL_VERTICAL_MARGIN;
    
    UILabel *label = [UILabel new];
    label.font = PILL_FONT;
    
    for (int i=0; i<pills.count; i++) {
        
        float pillWidth = [pills[i] boundingRectWithSize:label.frame.size
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:label.font}
                                              context:nil].size.width + PILL_WIDTH_MARGIN;
        
        
        currentWidthCount = currentWidthCount + pillWidth + PILL_SEPERATOR_MARGIN;
        
        if (currentWidthCount < [[UIScreen mainScreen] bounds].size.width - HORIZONTAL_MARGIN) {
            
            [self insertInScrollView:scrollView labelWithText:pills[i] andFrame:CGRectMake(initialOriginX, initialOriginY, pillWidth, PILL_HEIGHT)];
            
            initialOriginX = currentWidthCount;
        }
        else {
            
            initialOriginX = HORIZONTAL_MARGIN;
            currentWidthCount = HORIZONTAL_MARGIN;
            initialOriginY = initialOriginY + PILL_HEIGHT + PILL_SEPERATOR_MARGIN;
            
            [self insertInScrollView:scrollView labelWithText:pills[i] andFrame:CGRectMake(initialOriginX, initialOriginY, pillWidth, PILL_HEIGHT)];
            
            currentWidthCount = currentWidthCount + pillWidth + PILL_SEPERATOR_MARGIN;
            initialOriginX = currentWidthCount;
        }
    }
}

+ (void)insertInScrollView:(UIScrollView *)scrollView labelWithText:(NSString *)text andFrame:(CGRect)frame {

    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = PILL_COLOR;
    view.layer.cornerRadius = view.frame.size.height/2;
    view.alpha = 0.0f;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = text;
    label.font = PILL_FONT;
    label.textColor = [UIColor whiteColor];
    [view addSubview:label];

    [scrollView addSubview:view];
    
    [UIView animateWithDuration:ANIMATION_DURATION_PILL_IN delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^{
        view.alpha = 1.0;
    } completion:nil];
    
    delay = delay + PILL_ENTRANCE_DELAY;
}

@end
