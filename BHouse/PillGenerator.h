//
//  PillGenerator.h
//  BHouse
//
//  Created by Satraj Bambra on 2015-02-12.
//  Copyright (c) 2015 Bhouse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PillGenerator : NSObject

+ (void)addPills:(NSArray *)pills toScrollView:(UIScrollView *)scrollView;

@end
