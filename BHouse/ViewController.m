//
//  ViewController.m
//  BHouse
//
//  Created by Satraj Bambra on 2015-02-12.
//  Copyright (c) 2015 Bhouse. All rights reserved.
//

#import "ViewController.h"
#import "PillGenerator.h"

#define SAMPLE_PILLS_COUNTRY @[@"USA", @"Canada", @"England", @"Mexico", @"Jamaica", @"Kenya", @"Finland", @"India", @"Australia", @"Japan"]
#define SAMPLE_PILLS_SKILLS @[@"Objective-C", @"Swift", @"iOS", @"Unit Testing", @"Android", @"Node.js", @"UI/UX Development"]
#define SAMPLE_PILLS_SEASONS @[@"Summer", @"Fall", @"Winter", @"Autumn"]
#define SAMPLE_PILLS_CARS @[@"Infiniti", @"BMW", @"Honda", @"Toyota", @"Ford", @"Lamborghini", @"Aston Martin", @"Ferrari"]
#define SAMPLE_PILLS_TECH @[@"Medium", @"Twitter", @"Facebook", @"Uber", @"Instacart", @"Shopify", @"Shyp", @"Product Hunt"]

@interface ViewController () {
    NSArray *pills;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    pills = @[SAMPLE_PILLS_COUNTRY, SAMPLE_PILLS_SKILLS, SAMPLE_PILLS_SEASONS, SAMPLE_PILLS_CARS, SAMPLE_PILLS_TECH];
}

#pragma TextField Delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    [self clearScrollView];
    
    if ([textField.text stringByReplacingCharactersInRange:range withString:string].length > 0) {
         [PillGenerator addPills:pills[arc4random() % pills.count] toScrollView:self.scrollView];
    }
    return YES;
}

- (BOOL) textFieldShouldClear:(UITextField *)textField {
    
    [self clearScrollView];
    return YES;
}

- (void)clearScrollView {

    if (self.scrollView.subviews.count > 0) {
        [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
}

@end
